import { defineConfig } from "vite";

export default defineConfig({
    build: {
        outDir: "dist",
        rollupOptions: {
            input: ["./autoload.js", "./layout/index.less"],
            output: {
                assetFileNames: ({name}) => ({
                    "index.css": "expositio.css",
                })[name] || "assets/[name].[hash].[ext]",
                entryFileNames: ({name}) => ({
                    autoload: "expositio.js",
                })[name] || "[name].js",
            },
        },
    },
});
