// This file is part of Expositio.
//
// Expositio is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Expositio is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Expositio.  If not, see <https://www.gnu.org/licenses/>.

export const setClasses = (classes) => (el) => {
    for (const [className, present] of Object.entries(classes)) {
        if (present) {
            el.classList.add(className);
        } else {
            el.classList.remove(className);
        }
    }
};

export const toggleFullscreen = (el) => {
    if (document.fullscreenElement) {
        document.exitFullscreen();
    } else {
        el.requestFullscreen();
    }
};

/**
 * Run a main function when the document is done loading.
 */
export const bootstrap = (main) => {
    if (document.readyState === "loading") {
        const done = () => {
            document.removeEventListener("DOMContentLoaded", done);
            main();
        };
        document.addEventListener("DOMContentLoaded", done);
    } else {
        setTimeout(main);
    }
};
