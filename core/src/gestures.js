// This file is part of Expositio.
//
// Expositio is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Expositio is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Expositio.  If not, see <https://www.gnu.org/licenses/>.

import { id } from "@fpjs/overture/base";
import { pure } from "@fpjs/overture/algebras/applicative";
import { map } from "@fpjs/overture/algebras/functor";
import { concat } from "@fpjs/overture/algebras/semigroup";

import { Signal } from "@fpjs/signal";
const { filter, foldp } = Signal;

const gesture = (touches) => {
    const start = touches[0];
    const end = touches[touches.length-1];

    const xdelta = end.x - start.x;
    const ydelta = end.y - start.y;

    // require at least 10% movement for gestures
    const xmin = window.screen.availWidth / 10;
    const ymin = window.screen.availHeight / 10;

    if (Math.abs(xdelta) > xmin || Math.abs(ydelta) > ymin) {
        // don't track vertical swipes
        if (Math.abs(xdelta) < Math.abs(ydelta) + ymin) {
            return null;
        }

        if (xdelta <= -xmin) {
            return "swipe_to_left";
        }
        if (xdelta >= xmin) {
            return "swipe_to_right";
        }
    }
    return "tap";
};

export const trackGestures = () => {
    const touchState = {
        touches: [],
        gesture: null,
    };

    const events = pure (Signal.Signal) (null);
    window.addEventListener("touchstart", (e) => events.set(e));
    window.addEventListener("touchmove", (e) => events.set(e));
    window.addEventListener("touchend", (e) => events.set(e));
    window.addEventListener("touchcancel", (e) => events.set(e));

    const touchesToCoords = (touches) =>
          map (x => ({x: x.screenX, y: x.screenY})) ([...touches]);

    const handle = (e) => (state) => {
        if (e == null) {
            return state;
        }
        switch (e.type) {
        case "touchstart":
            return {
                ...state,
                touches: touchesToCoords(e.touches),
                gesture: null,
            };
        case "touchmove":
            return {
                ...state,
                touches: concat (state.touches) (touchesToCoords (e.touches))
            };
        case "touchend":
            return {
                ...state,
                touches: [],
                gesture: gesture (state.touches)
            };
        case "touchcancel":
            return {
                ...state,
                touches: [],
                gesture: null,
            };
        default: state;
        }
    };

    const touches = foldp (handle) (touchState) (events);
    return filter (id) (map (x => x.gesture) (touches));
};
