// This file is part of Expositio.
//
// Expositio is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Expositio is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Expositio.  If not, see <https://www.gnu.org/licenses/>.

import { compose, constant, id } from "@fpjs/overture/base";

import { over, set, Record, Indexed } from "@fpjs/overture/control/lens";
const _ = Record;
const ix = Indexed.ix;

import { App } from "@fpjs/signal";
const { transition } = App;

const inRange = ([l,u], i) => i >= l && i <= u;
const lower = ([l,_]) => l;

const navigate = (f) => ({slides, index, ...rest}) => {
    const current = slides[index];
    if (inRange (current.range, f(current.index))) {
        return {
            slides: over (ix (index)) (over (_.index) (f)) (slides),
            index,
            ...rest
        };
    } else if (inRange ([0, slides.length-1], f(index))) {
        const index_ = f (index);
        const forward = index_ > index;
        return {
            index: index_,
            slides: over (ix (index_)) (over (_.index) (forward ? (constant (0)) : id)) (slides),
            ...rest
        };
    }
    return {...rest, slides, index};
};

const changeSlide = (f) => ({slides, index, ...rest}) => {
    if (inRange ([0, slides.length-1], f (index))) {
        return {...rest, slides, index: f (index)};
    }
    return {...rest, slides, index};
};

const restartFragment = ({slides, index, ...rest}) => {
    const restart = (slide) => set (_.index) (lower(slide.range)) (slide);
    return {
        slides: over (ix (index)) (restart) (slides),
        index,
        ...rest,
    };
};

export const Actions = {
    next: transition (navigate (n => n + 1)),
    nextSlide: transition (changeSlide (n => n + 1)),
    nextSlideRestart: transition (compose (restartFragment) (changeSlide (n => n + 1))),
    previous: transition (navigate (n => n - 1)),
    previousSlide: transition (changeSlide (n => n - 1)),
    previousSlideRestart: transition (compose (restartFragment) (changeSlide (n => n - 1))),
    reset: transition (set (_.mode) ("presenting")),
    toggleOverviewMode: transition (over (_.mode) ((mode) => mode === "overview" ? "presenting" : "overview")),
    toggleContinuousMode: transition (over (_.mode) ((mode) => mode === "continuous" ? "presenting" : "continuous")),
};
