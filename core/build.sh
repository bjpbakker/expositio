#!/bin/sh

set -eo pipefail

oops() {
    echo "$@"
    exit 1
}

run() {
    if [ -z "$npm_package_name" ]; then
        npx --no-install "$@"
    else
        "$@"
    fi
}

pristine=0
clean=0

main() {
    for arg in "$@"; do
        case $arg in
            --clean )
                clean=1 ;;
            --pristine )
                pristine=1 ;;
            * )
                oops "Unknown argument: $arg"
        esac
    done

    if [ $pristine -ne 0 ]; then
        test -z "$(git status --porcelain --untracked-files=no)" || oops "Cannot proceed with dirty working dir"
    fi

    if [ $clean -ne 0 ]; then
        rm -rf dist/
    fi

    rm -rf dist/
    run vite build
}

main "$@"
