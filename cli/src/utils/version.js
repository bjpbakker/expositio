// This file is part of Expositio.
//
// Expositio is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Expositio is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Expositio.  If not, see <https://www.gnu.org/licenses/>.

export const Version = (major, minor, build) => ({
    major, minor, build,
    "fantasy-land/lte": (y) => major < y.major
        || (major === y.major && minor < y.minor)
        || (major === y.major && minor === y.minor && build <= y.build),
    toString: () => `${major}.${minor}.${build}`,
});

export const parseVersion = (s) => {
    if (s[0] === "~" || s[0] === "^") {
        s = s.substr(1);
    }
    const [major, minor, build] = s.split(".");
    return Version(major|0, minor|0, build|0);
};
