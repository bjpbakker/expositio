// This file is part of Expositio.
//
// Expositio is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Expositio is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Expositio.  If not, see <https://www.gnu.org/licenses/>.

import os from "os";

const indexHtml = ({
    title, slides,
    theme="white",
}) => `\
<!doctype html>
<html expositio="standalone">
  <head>
    <meta charset="utf-8" />
    <title>${title}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
    <script async src="./index.js" type="module"></script>
    <link rel="shortcut icon" href="data:image/x-icon;">
  </head>

  <body>
    <main id="expositio" data-theme="${theme}">
      <div class="slides">
${slides}
      </div>

      <footer>
        <aside class="attribution">Made with <a href="https://gitlab.com/bjpbakker/expositio">Expositio</a>.</aside>
        <div id="slide-progress"></div>
      </footer>
    </main>
  </body>
</html>
`;

const indexJs = ({
    layout="./index.less",
}) => `\
import * as Expositio from "@expositio/core";
import "${layout}";

Expositio.main ({
  "theme-switcher": {
    enable: true,
    "extra-themes": [],
  },
  // Function invoked when the expositio is ready. Useful for
  // initialization of additional tools (e.g. syntax highlighting)
  // and customizations.
  ready: (app) => {},
});
`;

const layoutLess = () => `\
@import "@expositio/core/layout";
`;

export default {
    "index.html": indexHtml,
    "index.js": indexJs,
    "layout.less": layoutLess,
};
