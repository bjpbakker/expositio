// This file is part of Expositio.
//
// Expositio is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Expositio is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Expositio.  If not, see <https://www.gnu.org/licenses/>.

import fs from "fs";
import {spawn} from "child_process";

import {compose, constant, flip} from "@fpjs/overture/base";
import {map} from "@fpjs/overture/algebras/functor";
import {chain} from "@fpjs/overture/algebras/chain";
import {pure} from "@fpjs/overture/algebras/applicative";
import {foldl} from "@fpjs/overture/algebras/foldable";
import {Callback} from "@fpjs/overture/control/callback";
import {set} from "@fpjs/overture/control/lens";

export const oops = (message, exitCode=1) => {
    process.stderr.write(message + "\n");
    process.exit(exitCode);
};

/**
 * Wrap a task function with printing it's name and status to track progress.
 *
 * A task function has the signature of `state -> Callback e state`.
 */
///:: (String, (a -> Callback e a), Options) -> a -> Callback e a
export const task = (name, f, {verbose=false}={}) => (state) => {
    process.stdout.write(`${name}.. ${verbose ? "\n" : ""}`);
    const ok = (x) => (verbose || process.stdout.write(`ok\n`), x);
    return map (ok) (f (state));
};

/**
 * Wrap a task function that collects data. It's name and collected value are
 * printed.
 *
 * Unlike `task` this takes a function `state -> Callback e a` and a lens that
 * is used to set the resulting value `a` in the state.
 */
export const collect = (name, lens, f, {show=true}={}) => (state) => {
    process.stdout.write(name + ".. ");
    const ok = (x) => (process.stdout.write(`${show ? String(x) : "ok"}\n`), x);
    return map (flip (set (lens)) (state)) (map (ok) (f (state)));
};

/**
 * Like `task` but uses an effectful predicate to determine whether the task
 * should be executed.
 */
///:: (String, (a -> Callback e Bool), (a -> Callback e a), Options) -> a -> Callback e a
export const optionalTask = (name, p, f, {verbose=false}={}) => (state) => {
    const run = (p_) => {
        process.stdout.write(name + ".. ");
        if (p_) {
            const ok = (x) => (verbose || process.stdout.write("ok\n"), x);
            return map (ok) (f (state));
        } else {
            const skip = (x) => (process.stdout.write("skipped\n"), x);
            return map (skip) (pure (Callback) (state));
        }
    };
    return chain (run) (p (state));
};

/** Run a series of sequential tasks, starting with the inital state. */
///:: [(a -> Callback e a)] -> a -> Callback e a
export const runTasks = (...tasks) => (initState) => {
    const z = pure (Callback) (initState);
    return foldl (flip (chain)) (z) (tasks);
};

/** Execute a command and return it's stdout. */
export const command = (cmd, args, options) => Callback(
    (done) => {
        let stdout = "", stderr = "";
        const proc = spawn(cmd, args, options);
        proc.stdout.on("data", (chunk) => { stdout += chunk.toString(); });
        proc.stderr.on("data", (chunk) => { stderr += chunk.toString(); });
        proc.on("close", (exitCode) => {
            if (exitCode !== 0) {
                done(`${cmd} ${args.join(" ")} failed with exit code ${exitCode}\n\n${stderr}`);
            } else {
                done(null, stdout);
            }
        });
    }
);

/**
 * Run a command with its IO handles (stdin, stdout, stderr) inherited from the
 * parent process.
 */
export const callCommand = (cmd, args, options) => Callback(
    (done) => {
        const proc = spawn(cmd, args, {...options, stdio: "inherit"});
        proc.on("close", (exitCode) => {
            if (exitCode !== 0) {
                done(`${cmd} ${args.join(" ")} failed with exit code ${exitCode}`);
            } else {
                done(null, null);
            }
        });
    }
);

export const readFile = (filename, options={}) => Callback(
    (done) => fs.readFile(filename, options, done)
);

export const writeFile = (filename, data, options={}) => Callback(
    (done) => fs.writeFile(filename, data, options, done)
);

export const unlinkFile = (filename) => Callback(
    (done) => fs.unlink(filename, done)
);

export const readStdin = (bufsize=undefined) => Callback(
    (done) => {
        const stdin = process.stdin;
        let data = Buffer.alloc(0);
        stdin.on("readable", (chunk) => {
            while (null !== (chunk = stdin.read(bufsize))) {
                data = Buffer.concat([data, chunk]);
            }
        });
        stdin.on("end", () => done(null, data));
    }
);

export const fromTry = (f) => Callback(
    (done) => {
        try {
            done(null, f());
        } catch (err) {
            done(err);
        }
    }
);
