// This file is part of Expositio.
//
// Expositio is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Expositio is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Expositio.  If not, see <https://www.gnu.org/licenses/>.

import {basename} from "path";

import {flip} from "@fpjs/overture/base";
import {pure} from "@fpjs/overture/algebras/applicative";
import {map, replace} from "@fpjs/overture/algebras/functor";
import {Callback, runCallback} from "@fpjs/overture/control/callback";
import {Record as _, over, set} from "@fpjs/overture/control/lens";

import yaml from "js-yaml";

import compile from "../mdast/compile.js";

import {
    oops,
    runTasks, task,
    readFile, writeFile, readStdin,
    fromTry,
} from "../utils/command.js";

import templates from "../utils/templates.js";

export default {
    synopsis: "generate an expositio from markdown",
    doc: `\
Usage: generate <FILE>

Generate an Expositio HTML page from markdown FILE, or read from stdin when FILE
is '-'.

Slides are automatically composed based on markdown headers. To specifically
mark slide content, use the \`::slide' container directive.
`,
    run: (flags) => {
        const [input] = flags;
        if (input == null) {
            oops("No input file was provided. Use '-' if you want to read from stdin.");
        }
        const inputName = (input !== "-") ? basename(input) : "STDIN";

        const runProgram = runCallback ((err, state) => {
            if (err != null) {
                oops(err);
            } else {
                process.stdout.write(`
Expositio is generated from ${inputName}.
`);
            }
        });

        const initState = {
            input,
        };

        const extractFrontmatter = (markdown) => Callback((done) => {
            const begin = 4;
            const end = markdown.indexOf("\n---\n", begin);
            if ((String(markdown.slice(0, begin)) !== "---\n") || end === -1) {
                return done(null, {
                    frontmatter: {},
                    markdown: markdown,
                });
            }

            done(null, {
                frontmatter: yaml.load(markdown.slice(begin, end)),
                markdown: markdown.slice(end + 5),
            });
        });

        const genExpositio = runTasks(
            task(
                `Read ${inputName}`,
                (state) => map (flip (set (_.markdown)) (state)) (
                    (state.input !== "-") ? readFile (state.input) : readStdin()
                )
            ),
            task(
                "Parse frontmatter",
                (state) => map (({...updated}) => ({...state, ...updated})) (
                    extractFrontmatter (state.markdown)
                )
            ),
            task(
                "Render slide deck",
                (state) => map (flip (set (_.html)) (state)) (
                    fromTry (() => compile(state.markdown))
                )
            ),
            task(
                "Generate index.html",
                (state) => map (flip (set (_.indexHtml)) (state)) (
                    pure (Callback) (templates['index.html']({
                        title: state.frontmatter.title || `${inputName} Expositio`,
                        slides: state.html,
                        theme: state.frontmatter.theme,
                        layout: state.frontmatter.layout,
                    }))
                )
            ),
            task(
                "Write index.html",
                ({indexHtml}) => replace (indexHtml) (writeFile("index.html", indexHtml)),
            ),
        );

        runProgram (genExpositio (initState));
    }
};
