// This file is part of Expositio.
//
// Expositio is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Expositio is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Expositio.  If not, see <https://www.gnu.org/licenses/>.

import fs from "fs";
import os from "os";
import path from "path";

import {fileURLToPath} from "url";

import {constant} from "@fpjs/overture/base";
import {map, replace, replicate} from "@fpjs/overture/algebras/functor";

import {Callback, runCallback} from "@fpjs/overture/control/callback";
import {Record as _} from "@fpjs/overture/control/lens";

import {
    oops, runTasks, task, collect, command, callCommand, readFile, writeFile
} from "../utils/command.js"
import templates from "../utils/templates.js";

const gitConfig = (key) =>
      map (result => result.split("\n")[0]) (command ("git", ["config", key]));

const packageJson = ({appname, authorName, authorEmail, cliPackageJson}) => {
    const author = `${authorName} <${authorEmail}>`;
    return {
        name: appname,
        version: "1.0.0",
        description: `${appname} expositio`,
        scripts: {
            "build": "vite build --emptyOutDir",
            "start": "vite serve",
        },
        author: author,
        license: "CC-BY-4.0",
        dependencies: {
            "@expositio/core": cliPackageJson["dependencies"]["@expositio/core"],
        },
        devDependencies: {
            "expositio": cliPackageJson["version"],
        },
    };
};

const indexHtml = ({appname, authorName, authorEmail, ...state}) =>
      templates["index.html"]({
          title: `${appname} expositio`,
          slides: `
        <section class="slide">
          <h2>${appname}</h2>
        </section>
`,
          ...state
      });

const outputDirectory = (flags) => {
    const index = flags.indexOf("--output-directory");
    return ~index ? flags[index+1] : ".";
};

export default {
    synopsis: "create a new expositio",
    doc: `\
Usage: init [--output-directory directory]

Scaffold a new Expositio project in the given directory (or current directory
when none was given). The required \`index.html' and \`index.js' are generated,
dependencies installed, and a Git repository is initialized.

The project name is derived from the name of the current directy. Author
information is extracted from Git.
`,
    run: (flags) => {
        const out = path.resolve(outputDirectory(flags));
        if (!fs.existsSync(out)) {
            oops(`${out} does not exist`);
        }
        if (!fs.statSync(out).isDirectory()) {
            oops(`${out} is not a directory`);
        }

        if (fs.existsSync(path.join(out, "package.json"))) {
            oops("package.json already exists; init does not overwrite existing projects.");
        }

        const writeFile_ = (filename, ...args) =>
              writeFile(path.join(out, filename), ...args);
        const callCommand_ = (cmd, args) =>
              callCommand(cmd, args, {cwd: out});

        const runProgram = runCallback ((err, state) => {
            if (err != null) {
                oops(err);
            } else {
                process.stdout.write(`
Expositio '${state.appname}' is created. For defaults the version is set to
1.0.0 and your work is licensed under CC BY 4.0. You can change these in
'package.json'.

Use 'npm start' to run ${state.appname}. Happy hacking!
`);
            }
        });

        const state = {
            appname: path.basename (out),
            authorName: "",
            authorEmail: "",
            theme: "white",
        };

        const initExpositio = runTasks(
            collect(
                "Get user name",
                _.authorName,
                constant (gitConfig ("user.name"))
            ),
            collect(
                "Get user email",
                _.authorEmail,
                constant (gitConfig ("user.email"))
            ),
            collect(
                "Read expositio-cli package.json",
                _.cliPackageJson,
                () => {
                    const __dirname = path.dirname(fileURLToPath(import.meta.url));
                    const packageJson = path.join(__dirname, "..", "..", "package.json");
                    return map (JSON.parse) (readFile(packageJson));
                },
                {show: false}
            ),
            task(
                "Init Git repository",
                replicate (command ("git", ["-C", out, "init"]))
            ),
            task(
                "Write package.json",
                (state) => replace (state) (
                    writeFile_ ("package.json", JSON.stringify(packageJson(state), null, 2) + os.EOL)
                )
            ),
            task(
                "Write layout.less",
                (state) => replace ({...state, layout: "./layout.less"}) (
                    writeFile_ ("layout.less", templates["layout.less"](state))
                )
            ),
            task(
                "Write index.js",
                (state) => replace (state) (writeFile_ ("index.js", templates["index.js"](state)))
            ),
            task(
                "Write index.html",
                (state) => replace (state) (writeFile_ ("index.html", indexHtml(state)))
            ),
            task(
                "Write .gitignore",
                replicate (writeFile_ (".gitignore", "/dist/\n/node_modules/\n"))
            ),
            task(
                "Install dependencies",
                replicate (callCommand_ ("npm", ["install", "--loglevel", "silent"])),
                {verbose: true}
            ),
            task(
                "Install dev dependencies",
                replicate (
                    callCommand_ ("npm", ["install", "--save-dev", "--save-exact", "--loglevel", "silent"].concat([
                        "less@4",
                        "vite@4",
                    ]))
                ),
                {verbose: true}
            ),
        );

        runProgram (initExpositio (state));
    }
};
