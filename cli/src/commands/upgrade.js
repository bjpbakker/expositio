// This file is part of Expositio.
//
// Expositio is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Expositio is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Expositio.  If not, see <https://www.gnu.org/licenses/>.

import fs from "fs";
import os from "os";
import path from "path";

import {compose} from "@fpjs/overture/base";
import {lift2, pure} from "@fpjs/overture/algebras/applicative";
import {chain} from "@fpjs/overture/algebras/chain";
import {map, replace, replicate} from "@fpjs/overture/algebras/functor";
import {lt} from "@fpjs/overture/algebras/ord";
import {
    Callback, error, recover, runCallback,
} from "@fpjs/overture/control/callback";
import {Record as _, view} from "@fpjs/overture/control/lens";

import {
    oops, runTasks,
    collect, task, optionalTask, callCommand,
    readFile, writeFile, unlinkFile,
} from "../utils/command.js"
import templates from "../utils/templates.js";
import {Version, parseVersion} from "../utils/version.js";


const ignoreFileMissing = recover (
    (e) => e.code === "ENOENT" ? pure (Callback) (false) : error (e)
);

const fileMatches = (filename) => (regex) => {
    const read = map ((buf) => buf.toString()) (readFile (filename));
    const matches = (contents) => regex.test(contents);
    return ignoreFileMissing (map (matches) (read));
};

const fileReplace = (filename) => (regex, replacement) => {
    const read = map ((buf) => buf.toString()) (readFile (filename));
    const write = (contents) => writeFile (filename, contents);
    const patch = (contents) => contents.replace(regex, replacement);

    return chain (write) (map (patch) (read));
};

const and = lift2 (x => y => x && y);
const not = map (x => !x);

const deleteKeys = (...keys) => (o) => Object.entries(o)
      .filter(([k, _]) => !keys.includes(k))
      .reduce((acc, [k, v]) => ({...acc, [k]: v}), {});

export default {
    synopsis: "upgrade an expositio project",
    doc: `\
Usage: upgrade

Automatically upgrade an expositio project with the changes in past releases.

The goal of this command is mainly to automate big changes and
incompatibilities. Minor and backwards compatible changes are printed as
deprecation warnings on the console.
`,
    run: (flags) => {
        if (!fs.existsSync("package.json")) {
            oops("No package.json; upgrade needs an existing project.");
        }

        const runProgram = runCallback ((err, state) => {
            if (err != null) {
                oops(err);
            } else {
                process.stdout.write(`
Upgrade for '${state.packageJson.name}' completed.

Further deprecation warnings may be printed on the console when you run the expositio.
`);
            }
        });

        const state = {
            effects: {
                writePackageJson: false,
                npmInstall: false,
            }
        };

        const upgrades = [
            optionalTask(
                "v0.7.0: package split",
                ({packageJson}) => pure (Callback) (packageJson.dependencies["expositio"] != null),
                (state) => {
                    const {expositio, ...deps} = state.packageJson.dependencies;
                    const packageJson = {
                        ...state.packageJson,
                        dependencies: {
                            ...deps,
                            "@expositio/core": expositio
                        },
                        devDependencies: {
                            ...state.packageJson.devDependencies,
                            "expositio": expositio
                        },
                    };
                    return pure (Callback) ({
                        ...state,
                        packageJson,
                        effects: {
                            ...state.effects,
                            writePackageJson: true,
                            npmInstall: true,
                        },
                    });
                }
            ),
            optionalTask(
                "v0.7.0: import Expositio from @expositio/core",
                () => fileMatches ("index.js") (
                    /(^|\n)import \* as Expositio from "expositio";(\n|$)/
                ),
                replicate (fileReplace ("index.js") (
                    /(^|\n)import (\* as Expositio) from "expositio";(\n|$)/g,
                    '$1import $2 from "@expositio/core";$3'
                ))
            ),
            optionalTask(
                "v0.7.0: @import @expositio/core/layout",
                () => fileMatches ("layout.less") (
                    /(^|\n)@import ['"]~?expositio\/layout/
                ),
                replicate (fileReplace ("layout.less") (
                    /(^|\n)@import (['"]~?)expositio\/layout/g,
                    "$1@import $2@expositio/core/layout"
                ))
            ),
            optionalTask(
                "v0.7.0: link stylesheet @expositio/core/layout",
                () => fileMatches ("index.html") (
                    /<link rel="stylesheet" href="node_modules\/expositio\/layout(\/.+)?\.less">/
                ),
                replicate (fileReplace ("index.html") (
                    /<link rel="stylesheet" href="node_modules\/expositio\/layout(\/.+)?\.less">/g,
                    '<link rel="stylesheet" href="node_modules/@expositio/core/layout$1.less">'
                ))
            ),
            optionalTask(
                "v0.7.0: script tag uses type module",
                () => fileMatches ("index.html") (
                    /<script async src=".\/[^"]+"><\/script>/
                ),
                replicate (fileReplace ("index.html") (
                    /<script async src=".\/([^"]+)"><\/script>/g,
                    '<script async src="./$1" type="module"><\/script>'
                ))
            ),
            optionalTask(
                "v0.7.0: rename @default-font-size to @base-font-size",
                () => fileMatches ("layout.less") (
                    /@default-font-size/
                ),
                replicate (fileReplace ("layout.less") (
                  /@default-font-size/g,
                  "@base-font-size"
                ))
            ),
            optionalTask(
                "v0.7.0: replace webpack specific imports",
                () => fileMatches ("layout.less") (
                    /(^|\n)@import ['"]~[^'"]+['"];/
                ),
                replicate (fileReplace ("layout.less") (
                    /(^|\n)@import (['"])~([^'"]+)['"];/g,
                    "$1@import $2$3$2;"
                ))
            ),
            optionalTask(
                "v0.7.1: set viewport to scale to fill the device display",
                () => fileMatches ("index.html") (
                    /<meta +name=['"]viewport['"] +content=['"]width=device-width, *initial-scale=1['"] *>/
                ),
                replicate (fileReplace ("index.html") (
                    /(<meta +name=['"]viewport['"] +content=['"]width=device-width, *initial-scale=1)(['"] *>)/,
                    "$1, viewport-fit=cover$2"
                ))
            ),
            optionalTask(
                "v0.9.0: install vite (replacing parcel)",
                ({packageJson}) => pure (Callback) (
                    (packageJson.devDependencies||{})["parcel"] != null
                ),
                (state) => {
                    const cleanups = [
                        fileReplace (".gitignore") (
                            /(^|\n).(parcel-)?cache\/(\n|$)/,
                            "$1"
                        ),
                        ignoreFileMissing (unlinkFile (".parcelrc")),
                    ];
                    const cleanup = cleanups.reduce((f, g) => chain (replicate (f)) (g));

                    const cleanupDevDependencies = deleteKeys (
                        "parcel", "@expositio/parcel-config", "@parcel/transformer-less",
                        "@babel/core", "@babel/preset-env",
                    );
                    const cleanupPackageJson = deleteKeys ("babel", "browserslist", "htmlnano");

                    return replace ({
                        ...state,
                        packageJson: {
                            ...cleanupPackageJson (state.packageJson),
                            scripts: {
                                "build": "vite build --emptyOutDir",
                                "start": "vite serve",
                            },
                            devDependencies: {
                                ...cleanupDevDependencies (state.packageJson.devDependencies),
                                "vite": "^6.x",
                            },
                        },
                        effects: {
                            ...state.effects,
                            writePackageJson: true,
                            npmInstall: true,
                        },
                    }) (cleanup);
                }
            ),
        ];

        const upgradeExpositio = runTasks (
            collect(
                "Read Expositio package.json",
                _.packageJson,
                () => map (JSON.parse) (readFile("package.json")),
                {show: false}
            ),
            ...upgrades,
            optionalTask(
                "Write package.json",
                compose (pure (Callback)) (view (_.effects.writePackageJson)),
                (state) => replace (state) (
                    writeFile ("package.json", JSON.stringify(state.packageJson, null, 2) + os.EOL)
                )
            ),
            optionalTask(
                "Apply package.json changes",
                compose (pure (Callback)) (view (_.effects.npmInstall)),
                replicate (callCommand ("npm", ["install", "--loglevel", "silent"])),
                {verbose: true}
            ),
        );
        runProgram (upgradeExpositio (state));
    }
};
