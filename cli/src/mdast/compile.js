// This file is part of Expositio.
//
// Expositio is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Expositio is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Expositio.  If not, see <https://www.gnu.org/licenses/>.

import {fromMarkdown} from "mdast-util-from-markdown";
import {directive} from "micromark-extension-directive";
import {directiveFromMarkdown} from "mdast-util-directive";
import {toHast} from "mdast-util-to-hast";
import {toHtml} from "hast-util-to-html";

import {compose} from "@fpjs/overture/base";

import {groupSlides} from "./slides.js";

const parse = (text) => fromMarkdown(text, {
    extensions: [directive()],
    mdastExtensions: [directiveFromMarkdown()],
});

const transform = groupSlides;

const tohast = (node) => toHast (node, {
    allowDangerousHtml: true,
});
const tohtml = (tree) => toHtml (tree, {
    allowDangerousHtml: true,
});

const render = compose (tohtml) (tohast);

export default (x) => render (transform (parse(x)));
