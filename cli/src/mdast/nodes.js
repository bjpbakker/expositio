// This file is part of Expositio.
//
// Expositio is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Expositio is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Expositio.  If not, see <https://www.gnu.org/licenses/>.

import {fromMaybe, toMaybe} from "@fpjs/overture/data/maybe";
import {map} from "@fpjs/overture/algebras/functor";

const maybeToList = (a) => fromMaybe ([]) (map (x => [x]) (a));

export default {
    slide: (children, {tag, "class": classes=null, ...hProperties}={}) => ({
        type: "slide",
        children,
        data: {
            hName: "section",
            hProperties: {
                "class": ["slide", ...maybeToList(toMaybe(classes))].join(" "),
                "data-slide-tag": tag,
                ...hProperties,
            },
        },
    }),
};
