// This file is part of Expositio.
//
// Expositio is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Expositio is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Expositio.  If not, see <https://www.gnu.org/licenses/>.

import {constant} from "@fpjs/overture/base";
import {foldl} from "@fpjs/overture/algebras/foldable";
import {map} from "@fpjs/overture/algebras/functor";
import {concat} from "@fpjs/overture/algebras/semigroup";
import {equals} from "@fpjs/overture/algebras/setoid";

import nodes from "./nodes.js";

export const groupSlides = (tree) => {
    const group = ({acc, work}) => (node) => {
        switch (node.type) {
        case "heading": {
            if (work.length) {
                return {acc: concat (acc) ([work]), work: [node]};
            } else{
                return {acc, work: [node]}
            }
        }
        case "containerDirective": {
            if (node.name !== "slide") {
                return {acc, work: concat (work) ([node])};
            }
            const [x, ...xs] = node.children;
            const head = {...x, attributes: node.attributes};
            return {
                acc: concat (
                    concat (acc) (work.length ? [work] : [])
                ) ([[head, ...xs]]),
                work: []
            };
        }
        default:
            return {acc, work: concat (work) ([node])};
        }
    };

    const build = ([head, ...content]) => {
        const {"data-slide-tag": tag, ...hProperties} = {
            ...head.attributes,
        };
        const heading = {
            ...head,
            children: head.children,
        };
        return nodes.slide([heading, ...content], hProperties);
    };

    const {acc, work} = foldl (group) ({acc: [], work: []}) (tree.children);
    return {
        ...tree,
        children: map (build) (work.length ? (concat (acc) ([work])) : acc),
    };
};
