#!/usr/bin/env node

// This file is part of Expositio.
//
// Expositio is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Expositio is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Expositio.  If not, see <https://www.gnu.org/licenses/>.

import {gt} from "@fpjs/overture/algebras/ord";
import {patchBuiltins} from "@fpjs/overture/patches";

import {oops} from "../src/utils/command.js";
import {Version, parseVersion} from "../src/utils/version.js";

import {default as generate} from "../src/commands/generate.js";
import {default as init} from "../src/commands/init.js";
import {default as upgrade} from "../src/commands/upgrade.js";

const requiredNodeVersion = parseVersion("10.0");

const commandNotFound = (cmd) =>
      oops(`expositio: '${cmd}' is not a known command. See 'expositio --help'.`);

const commands = {
    generate,
    init,
    upgrade,
    help: {
        synopsis: "print usage instructions",
        run: (flags) => {
            if (!flags.length) {
                console.log(usage);
                return;
            }

            const [cmd] = flags;
            const command = commands[cmd];
            if (!command) {
                commandNotFound(cmd);
            }

            console.log(`\
${cmd} - ${command.synopsis}

${command.doc || ""}
`.trim());
        },
    },
};

const usage = `\
usage: expositio <command> [<options>]

Commands:

${Object.entries(commands)
        .map(([name, {synopsis}]) => "   " + name + " \t" + synopsis)
        .join("\n")}`;

function main() {
    const nodeVersion = parseVersion(process.versions.node);
    if (gt (requiredNodeVersion) (nodeVersion)) {
        oops(`You run Node.js v${nodeVersion}. You need at least ${requiredNodeVersion} to run Expositio.`);
    }

    const [cmd, ...flags] = process.argv.slice(2);
    if (cmd === "-h" || cmd === "--help") {
        console.log(usage);
        process.exit(0);
    }

    if (!cmd) {
        oops(usage);
    }

    const command = commands[cmd];
    if (!command) {
        commandNotFound(cmd);
    }
    command.run(flags);
}

patchBuiltins();
main();
