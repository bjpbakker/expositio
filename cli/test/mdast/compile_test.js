// This file is part of Expositio.
//
// Expositio is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Expositio is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Expositio.  If not, see <https://www.gnu.org/licenses/>.

import {describe, it} from "mocha";
import {assert} from "chai";

import compile from "../../src/mdast/compile.js";

const verify = (input, fragment) => {
    const r = compile(input);
    assert.include(r.replace(/\n/g, ""), fragment);
};

describe ("slide transformation", () => {
    describe ("automatic slide grouping", () => {
        it ("wraps headings in slide", () => {
            verify(
                '# One\n## Two',
                '<section class="slide"><h1>One</h1></section>'
                    + '<section class="slide"><h2>Two</h2></section>'
            );
        });

        it ("adds content to slide", () => {
            verify(
                '# One\n## Two\nContent\n## Three',
                '<section class="slide"><h1>One</h1></section>'
                    + '<section class="slide"><h2>Two</h2><p>Content</p></section>'
                    + '<section class="slide"><h2>Three</h2></section>'
            );
        });
    });

    describe ("container directive", () => {
        it ("allows custom grouping", () => {
            verify(
                ':::slide\n# One\n## Two\n:::',
                '<section class="slide"><h1>One</h1><h2>Two</h2></section>'
            );
        });

        it ("starts new slide with following (non-heading) content", () => {
            verify(
                ':::slide\n# One\n:::\nContent',
                '<section class="slide"><h1>One</h1></section>'
              + '<section class="slide"><p>Content</p></section>'
            );
        });

        it ("preserves header grouping", () => {
            verify(
                '# One\n:::slide\n## Two\n:::',
                '<section class="slide"><h1>One</h1></section>'
              + '<section class="slide"><h2>Two</h2></section>'
            );
        });

        it ("ignores non-slide directives", () => {
            verify(
                ':::xyz\n# One\n:::\nContent\n# Two',
                '<section class="slide"><div><h1>One</h1></div><p>Content</p></section>'
            );
        });

        describe ("using attributes", () => {
            it ("defines classes", () => {
                verify(
                    ':::slide{.x.y}\n# One\n:::',
                    '<section class="slide x y"><h1>One</h1></section>'
                );
            });

            it ("sets data-slide-tag from tag", () => {
                verify(
                    ':::slide{tag=my-slide}\n# One\n:::',
                    '<section class="slide" data-slide-tag="my-slide"><h1>One</h1></section>'
                );
            });

            it ("rejects data-slide-tag", () => {
                verify(
                    ':::slide{data-slide-tag=my-slide}\n# One\n:::',
                    '<section class="slide"><h1>One</h1></section>'
                );
            });
        });
    });

    describe ("markdown", () => {
        it ("allows HTML tags", () => {
            verify(
                '# Title\n<div class="window"><span class="title">My Window</span></div>\n',
                '<section class="slide"><h1>Title</h1><div class="window"><span class="title">My Window</span></div>'
            );
        });
    });
});
