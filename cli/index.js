import * as Expositio from "@expositio/core";

export const main = (...args) => {
    console.warn(`\
DEPRECATED: use 'import * as Expositio from "@expositio/core"' instead.

Expositio is split into a command-line utilities package ('expositio') and the
core presentation framework ('@expositio/core'). Slide decks should import from
'@expositio/core' directly.
`);
    return Expositio.main(...args);
};
