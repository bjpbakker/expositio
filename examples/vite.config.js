import { readdirSync } from "fs";
import { extname, resolve } from "path";
import { defineConfig } from "vite";
import { directoryPlugin } from "vite-plugin-list-directory-contents";

const dropExt = (s) => s.replace(RegExp(`${extname(s)}$`), "");

export default defineConfig({
    build: {
        outDir: "dist",
        rollupOptions: {
            input: readdirSync(__dirname)
                .filter((x) => x.match(/\.html$/))
                .map((x) => [dropExt(x), x])
                .reduce((acc, [name, file]) => ({...acc, [name]: file}), {}),
        },
    },
    plugins: [directoryPlugin({ baseDir: __dirname })],
});
