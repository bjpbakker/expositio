# Releases

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic
Versioning](https://semver.org/spec/v2.0.0.html).

## [unreleased]

### Fixed

- Positioning and color use of modal hints.

### Changed

- Links now have a dashed bottom border by default to improve discoverability.

## [v0.9.0] - 2024-07-09

### Added

- `expositio generate` to generate a slide deck from Markdown input. Only
  straigh-forward generation of HTML is supported in this first
  version. Advanced slide features (like terminal and codesheets) are not yet
  supported. See `expositio help generate` for usage instructions.
- Add 'continuous' mode that gives a continuous view of slides by allowing to
  scroll through the entire deck. This happens to be a convenient layout for
  printing the full slide deck too. Use F3 to toggle.
- Support different slide layouts through the `data-slide-template` attribute on
  the `.slide` tag. Builtin templates are:
  * `poly` for a general purpose layout; this was the only layout in previous
    versions and is still default.
  * `content` for a block of content (usually text or a picture) on a slide.
  * `columns` for a 2-column layout.
  These templates support an optional title and subtitle.

### Fixed

- Prioritize woff2 fonts over truetype to decrease download size.
- Print slides as they are rendered on screen. Hide the theme selection from
  printing.

### Changed

- Require nodejs 18 or higher (possibly breaking).
- Introduce vite and replace parcel (possible breaking). Unless using parcel
  directly, this requires no changes.
- Scrollbars on webkit browsers are smaller and matching the theme.

### Removed

- Deleted deprecated less variables (see v0.8.0).

## [v0.8.2] - 2023-04-25

### Fixed

- Fix `expositio init` so that it can be started without changes.

### Changed

- `expositio init` now can be passed a different output directory.
  E.g. `expositio init --output-directory my-project`.

## [v0.8.1] - 2023-04-12

### Added

- `.quote` style class to format inline and block quotes.

### Fixed

- Set default `line-height` (1.6em) on all slide text, not just paragraphs.

## [v0.8.0] - 2023-03-14

### Added

- Introduce split content in `window`, both horizontally (`hsplit`) and
  vertically (`vsplit`). Splits are controlled with `pane`s where each pane
  contains different content. To properly align content the `window` must get
  specific size, for which `normal` and `maximized` are introduced. See
  `examples/terminal.html` for example usages.
- A `modeline` can be added to a `pane` to show some additional information
  about the content of that pane.
- Slide sizes can be overridden per slide with CSS variables `--slide-h` and
  `--slide-w`. Boundaries allow for 100vw and 100vh minus footer height (both at
  top and bottom, to properly center content).
- Provide woff2 versions of included fonts.

### Fixed

- Fix slide scaling in overview mode (F2).
- Fix initialization of default white theme when no theme is set.
- Fix placement of warning banner when no theme is set.
- Terminal is scaled to screen size on small devices

### Changed

- Defaults have improved to make styling with simple utility classes more
  easy. Headings are now rendered in regular font-weight by default.
- Some utility classes are provided; see `core/layout/modules/utilities.less`.
- A `window` sizes to fit its content by default. To constrain its size class
  `normal` can be added (especially useful when splitting `pane`s). To utilize
  all slide space class `maximized` can be added.
- A `terminal` renders 80 characers in width and 24 lines in height by
  default. The `@term-lines` variable is replaced with CSS variable
  `--term-lines` to define the number of lines drawn in a terminal (when not
  maximized). A new CSS variable `--term-cols` is added to define the number
  of characters per line.
- Less variable `@term-border-radius` is renamed to `@window-border-radius`.
- Terminal related less variables are now more consistently named. This involves
  the following renames. Old variable names are now deprecated but still fully
  supported. They will however be removed in a future release.
  * `@terminal-fg` -> `@term-fg`
  * `@terminal-bg` -> `@term-bg`
  * `@linum-fg` -> `@term-linum-fg`
  * `@ps1-fg` -> `@term-ps1-fg`
  * `@ask-fg` -> `@term-ask-fg`
  * `@cursor-bg` -> `@term-cursor-bg`
- The cursor color used by the typewrited can now be controlled with CSS
  variable `--cursor-bg`.

## [v0.7.1] - 2022-11-28

### Added

- Add `theme-switcher.extra-themes` configuration option to allow adding custom
  themes to the builtin theme switcher.
- Add `build` script to newly initialized projects.
- Add `ready` function to newly initialized projects.

### Fixed

- Fix `expositio upgrade` when having no `devDependencies` in a project.
- Fix gitignore for `.parcel-cache/` and upgrade `.cache/` to `.parcel-cache/`.
- Fix replacement of all webpack-style imports in custom `layout.less`.

### Changed

- Theme switcher uses muted color for theme seperator lines.
- Prevent unnecessary renders when state is not updated.

## [v0.7.0] - 2022-06-11

### Added

- `expositio upgrade`: introduce automated updates
- Experimental video autoplay support

### Changed

- Split packages: `expositio` and `@expositio/core`
- `expositio init`: works with nodejs 18
- Require nodejs 14 or higher (possibly breaking)
- Upgrade to parcel 2.x (possibly breaking)
- Replace `normalize.css` with `modern-normalize`
- Set base font-size (2em) for all slide content (possible breaking)

## [v0.6.0] - 2022-02-11

### Added

- Expositio.main: call `ready` callback in options when ready

### Fixed

- code blocks: fix default alignment
- terminal: preserve top padding when scrolling

### Changed

- Enable theme-switcher by default when no `data-theme` is set. The options
  passed to `Expositio.main` still override the default.
- Reset fragments when using swipe gestures to navigate on touch devices.

## [v0.5.1] - 2021-11-01

### Added

- Introduce `expositio="standalone"` on document root as part of
  `expositio-theme` replacement. Migration instructions are printed on console
  (warn) when applicable.

### Fixed

- Use consistent slide font-size for paragraphs and lists
- `expositio init`: work-around possible `Invalid Version` in
  `@babel/preset-env`

### Changed

- Default to white theme when no theme is set and theme-switcher is disabled.
- `expositio init`: lock parcel version to 1.12.4

## [v0.5.0] - 2021-10-23

### Added

- Terminal: support scrolling
- Warn when `expositio-theme` is used
- `expositio help`: print command-line usage instructions
- Provide default image sizing
- Navigation on touch devices through tap and gestures

### Changed

- Update less to 4.1.2 (fixes https://github.com/less/less.js/pull/3602)

## [v0.4.1] - 2021-05-29

### Fixed

- Fix non-compatible changes in less 4.x, so both 3.x and 4.x are supported

### Changed

- `expositio init`: lock version of less to 3.12.2 because less >= 3.13.0 is
  broken. See see also https://github.com/less/less.js/pull/3602

## [v0.4.0] - 2020-11-29

### Added

- Support ordered lists (`ol`) on slides

### Fixed

- Fix positioning when using framents

### Changed

- Enable theme-selector by default
- Heading tags `h1..h6` are styled identical when used on slides
- Remove default margins on `<pre>`
- Increase font size of paragraph text on slide
- Replace `parcel-bundler` with `parcel`

## [v0.3.1] - 2020-07-28

### Fixed

- Fix disabling theme-selector with autoload

## [v0.3.0] - 2020-06-08

### Added

- New themes: `nord-bright` and `nord-dark`
- Reflect active slide tag on root element
- Log warnings for deprecated things

### Fixed

- Improve code rendering in terminal windows
- Fix rendering terminal without titlebar

### Changed

- Use `#expositio` for root element
- Typewriter: support up to 99 characters

## [v0.2.1] - 2020-04-16

### Fixed

- Scaffolding: fix theme-switcher

### Changed

- Scaffolding: change default license to `CC-BY-4.0`

## [v0.2.0] - 2020-04-10

### Added

- Theme selector for builtin themes
- `black` theme

### Changed

- Extract terminal themes to `layout/themes/terms`
- Rename theme `simple` to `white` (possible breaking)

## [v0.1.3] - 2020-04-07

### Added

- Document configuration options

### Fixed

- Minor layout issues

## [v0.1.2] - 2020-04-07

### Fixed

- Minor layout issues

## [v0.1.1] - 2020-04-07

### Added

- Presentation code imported from [git –productive](https://gitlab.com/bjpbakker/git-productive)
- Brief README documentation
- Basic theme support

[unreleased]: https://gitlab.com/bjpbakker/expositio/-/compare/v0.9.0...main
[v0.9.0]: https://gitlab.com/bjpbakker/expositio/-/compare/v0.8.2...v0.9.0
[v0.8.2]: https://gitlab.com/bjpbakker/expositio/-/compare/v0.8.1...v0.8.2
[v0.8.1]: https://gitlab.com/bjpbakker/expositio/-/compare/v0.8.0...v0.8.1
[v0.8.0]: https://gitlab.com/bjpbakker/expositio/-/compare/v0.7.1...v0.8.0
[v0.7.1]: https://gitlab.com/bjpbakker/expositio/-/compare/v0.7.0...v0.7.1
[v0.7.0]: https://gitlab.com/bjpbakker/expositio/-/compare/v0.6.0...v0.7.0
[v0.6.0]: https://gitlab.com/bjpbakker/expositio/-/compare/v0.5.1...v0.6.0
[v0.5.1]: https://gitlab.com/bjpbakker/expositio/-/compare/v0.5.0...v0.5.1
[v0.5.0]: https://gitlab.com/bjpbakker/expositio/-/compare/v0.4.1...v0.5.0
[v0.4.1]: https://gitlab.com/bjpbakker/expositio/-/compare/v0.4.0...v0.4.1
[v0.4.0]: https://gitlab.com/bjpbakker/expositio/-/compare/v0.3.1...v0.4.0
[v0.3.1]: https://gitlab.com/bjpbakker/expositio/-/compare/v0.3.0...v0.3.1
[v0.3.0]: https://gitlab.com/bjpbakker/expositio/-/compare/v0.2.1...v0.3.0
[v0.2.1]: https://gitlab.com/bjpbakker/expositio/-/compare/v0.2.0...v0.2.1
[v0.2.0]: https://gitlab.com/bjpbakker/expositio/-/compare/v0.1.3...v0.2.0
[v0.1.3]: https://gitlab.com/bjpbakker/expositio/-/compare/v0.1.2...v0.1.3
[v0.1.2]: https://gitlab.com/bjpbakker/expositio/-/compare/v0.1.1...v0.1.2
[v0.1.1]: https://gitlab.com/bjpbakker/expositio/-/tags/v0.1.1
