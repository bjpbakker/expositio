[![pipeline status](https://gitlab.com/bjpbakker/expositio/badges/main/pipeline.svg)](https://gitlab.com/bjpbakker/expositio/-/commits/main)
[![npm package](https://img.shields.io/npm/v/expositio.svg)](https://www.npmjs.com/package/expositio)

# Expositio

A simple and lean presentation tool.

## About

Expositio is a presentation tool that allows to build and package presentations
as modern javascript applications.

The design phylosophy is to build a fast and reliable presentation, by utilizing
modern CSS (through `less`) as much as possible. Only navigation features are
controlled with javascript, using a reactive programming style.

## Getting started

The simplest way to create a new presentation is to use scaffolding of the
`expositio` command line tool.

```
[my-presentation]$ npx expositio@latest init
[my-presentation]$ npm start
```

## Upgrading

To upgrade from previous versions, update the `expositio` and `@expositio/*`
packages in `package.json` and install them. Then run the Expositio `upgrade`
command to apply some automated updates.

```
[my-presentation]$ ncu --filter '/@?expositio/'
[my-presentation]$ npm install
[my-presentation]$ npx expositio upgrade
```

Then start the presentation. There may be some remaining deprecation warnings
printed on the console.

When running multiple major versions behind, the best aproach is to update and
fix the deprecation warnings per major release.

A changelog listing the major changes is kept in `RELEASES.md`.

## Creating slides

Slides and fragments are marked with style classes. A slide is a single page in
the presentation. Fragments mark the boundaries of elements that become visisble
at a time. I.e. fragments help control navigation flow, to prevent revealing the
full slide content at once.

The slide desk is contained is an element with the `slides` class. Every slide
is marked by the `slide` class. Fragments are marked with `fragment`.

For an example of showing bullet points one-by-one, see
`examples/framents.html`.

### Terminal

The builtin terminal view allows for using (animated) terminals in a presention,
to show use of shell commands. An example is shown in the
`examples/terminal.html` example.

### Code

Code fragments can be shows in the terminal view. See `examples/code.html` for
an example.

### Videos

Video support is very experimental. Autoplay and pause is done for videos marked
with `data-autoplay` only. Videos can be directly on a slide, or contained in a
fragment.

To properly render video sizes, for some browsers you may need to explicitly
override video sizes in CSS.

See `examples/video.html` for an example.

## Configuration

### Progress bar

Presentation process can be shown in an animated bar, at the bottom of the
window. A process bar is rendered inside the html element with id
`slide-progress`, if any.

This is enabled by default when using `expositio init`. To disable, remove the
`slide-process` container.

### Slide numbers

Expositio can also show the presentation process in terms of numbers
(i.e. 1/_n_), in an html element with id `slidenum`.

To enable this, add the element to your document. While you can fully customize
the element tag and its position in the document, the included themes expect an
inline element (e.g. `span` or `em`) in the `footer`.

### Theme switcher

By default a theme switcher will be shown in the top-left corner, allowing to
switch between builtin themes. When locking a specific theme using the
`data-theme` attribute (see below) the switcher is hidden by default.

The switcher can be explicitly enabled and disabled with the `theme-switcher`
attribute in the options passed to `Expositio.main`.

## Themes

Builtin themes can be chosen with the `data-theme` attribute on the
`#expositio` presention root tag.

All Expositio styling is contained within the `#expositio` root element, so
slides can be combined with additional content on the page. As commonly the page
consists of Expositio alone, the attribute `expositio="standalone"` can be set
on the document root to take control of the full document body. `expositio init`
generates such a standalone presentation.

### Custom theme

Of course you can also create a custom theme. Import the `base` builtin theme,
pick a terminal theme, customize any variable and add your own styles.

A good start would be to customize the default foreground (`@default-fg`) and
background (`@default-bg`) colors, and (optionally) set a highlight color
(`@highlight-fg`). The `base` theme derives the rest of the colors, but of
course any variable can be overridden by the custom theme.

For example:

```less
#expositio[data-theme="my-theme"] {
    @import (multiple) "@expositio/core/layout/themes/base";
    @import "@expositio/core/layout/themes/terms/xterm";

    @default-bg: #fff;
    @default-fg: #000;
    @highlight-fg: #00ced1;
}
```

Then add your theme to the builtin theme selector to allow switching between
your custom theme and other builtin themes.

``` javascript
Expositio.main ({
  "theme-switcher": {
    enable: true,
    extra-themes: ["my-theme"],
  },
});
```

When you create a nice theme, a PR is much appreciated :)

## Developing

To get set for developing on Expositio, clone this repository and install the
npm dependencies. Then run an initial build to make sure everything works.

```
[expositio]$ npm install -ws
[expositio]$ npm -ws --if-present run build
```

You can then run the examples.

```
[expositio]$ npm start -w examples
```

## License

Copyright (C) Bart Bakker.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <https://www.gnu.org/licenses/>.
